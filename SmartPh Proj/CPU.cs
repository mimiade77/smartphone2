﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPh_Proj
{
  public class CPU
  {
    public string Make { get; set; }

    public string Model { get; set; }

    public decimal Clockspeed { get; set; }
  }
}
