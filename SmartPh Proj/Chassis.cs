﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPh_Proj
{
  public class Chassis
  {
    public decimal Length { get; set; }

    public decimal Width { get; set; }

    public decimal Height { get; set; }
  }
}
