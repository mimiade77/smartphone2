﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPh_Proj
{
  public class Battery
  {
    public int KwH { get; set; }

    public bool IsRemovable { get; set; }

    public int MaxTalkTime { get; set; }

  }
}
