﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPh_Proj
{
  public class SmartphoneClass
  {
    public int ScreenWidth { get; set; }

    public int ScreenHeight { get; set; }

    public CPU Cpu { get; set; }

    public Storage Storage { get; set; }

    public Camera FrontCamera { get; set; }

    public Camera RearCamera { get; set; }

    public Apps Apps { get; set; }

    public Battery Battery { get; set; }

    public Camera Camera { get; set; }

    public Chassis Chassis { get; set; }

    public OpSys OpSys { get; set; }

    public RadioFeat RadioFeat { get; set; }

    public Ram Ram { get; set; }

    public Screen Screen { get; set; }

    public decimal ScreenSizeIn { get; set; }

    public decimal ScreenSizeMm { get; set; }

    public string Make { get; set; }

    public string Model { get; set; }

    public decimal FrontCameraMegaPixels { get; set; }

    public decimal RearCameraMegaPixels { get; set; }

    public override string ToString()
    {
      var sb = new StringBuilder();

      sb.AppendFormat("Make: {0}", this.Make);
      sb.AppendFormat("\r\nModel:{0}", this.Model);
      sb.AppendFormat("\r\nRam: {0} MB", this.Ram.Size);

      return sb.ToString();
    }
  }
}







   