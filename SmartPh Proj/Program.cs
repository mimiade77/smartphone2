﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartPh_Proj.Utility;

namespace SmartPh_Proj
{
  class Program
{
    static void Main(string[] args)
    {
      var sc = new SmartphoneClass();

      
      //set all the properties
     
      sc.Make = "Samsung";
      sc.Model = "Sport Ed";
      sc.ScreenHeight = 1920;
      sc.ScreenSizeIn = (decimal)8;
      sc.ScreenWidth = 1080;
      sc.Ram = new Ram();
      sc.Ram.Size = (decimal)1210;
      sc.Screen = new Screen();
      sc.Screen.IsTouchScreen = true;


      //Smartphone.Models.SmartphoneClass
      Console.WriteLine(sc.ToString());

      Console.WriteLine("**** My Smartphone ****");
      Console.WriteLine();
      Console.WriteLine("Make: {0}, Model: {1}", sc.Make, sc.Model);
      Console.WriteLine("Screen Size: {0} in. ({1} mm)", sc.ScreenSizeIn, sc.ScreenSizeMm);
      Console.WriteLine("Screen Resolution: {0} x {1} pixels", sc.ScreenHeight, sc.ScreenWidth);
      Console.WriteLine("Rear Camera: {0} MP", sc.RearCameraMegaPixels);
      Console.WriteLine("Front Camera: {0} MP", sc.FrontCameraMegaPixels);
      Console.WriteLine();
      Console.WriteLine("**** Press the AnswerKey ****");
      Console.ReadKey();
    }
  }
}
