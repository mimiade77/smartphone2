﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPh_Proj
{
 public class Screen
  {
   public decimal Size { get; set; }

   public int ResolutionWidth { get; set; }

   public int ResolutionHeight { get; set; }

   public int PixelsPerInch { get; set; }

   public bool IsTouchScreen = true;
  }
}
